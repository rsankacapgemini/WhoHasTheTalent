package com.assignment.WhoHasTheTalent.WhoHasTheTalent;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistTest {

	@Test
	public void testGetUnionId() {
		assertEquals(4323, new Vocalist(4323, "F").getUnionId());
	}

	@Test
	public void testAnnounce() {
		assertEquals("I Sing in the key of - F - 4323", new Vocalist(4323, "F").announce());
	}

	@Test
	public void testGetKey() {
		assertEquals("F", new Vocalist(4323, "F").getKey());
	}

	@Test
	public void testAnnounceInt() {
		assertEquals("I Sing in the key of - F - at the volume 4 - 4323", new Vocalist(4323, "F").announce(4));
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testAnnounceInt1() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid voulume range is from 1 to 10");
		new Vocalist(14323, "F").announce(234);
	}

}
