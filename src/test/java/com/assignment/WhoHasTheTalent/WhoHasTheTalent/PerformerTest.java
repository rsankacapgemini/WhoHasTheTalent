package com.assignment.WhoHasTheTalent.WhoHasTheTalent;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

public class PerformerTest {

	@Test
	public void testGetUnionId() {
		assertEquals(234, new Performer(234).getUnionId());
	}

	@Test
	public void testGetUnionId1() {
		assertNotEquals(-23, new Performer(234).getUnionId());
	}

	@Test
	public void testAnnounce1() {
		assertEquals("234 - performer", new Performer(234).announce());
	}

	@Test
	public void testAnnounce2() {
		assertEquals("7654 - performer", new Performer(7654).announce());
	}

	@Test
	public void testAnnounce3() {
		assertEquals("2345 - performer", new Performer(2345).announce());
	}

	@Test
	public void testAnnounce4() {
		assertEquals("7623 - performer", new Performer(7623).announce());
	}

	@Test
	public void testAnnounce5() {
		assertNotEquals("17623 - performer", new Performer(7623).announce());
	}

}
