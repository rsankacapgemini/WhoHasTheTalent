package com.assignment.WhoHasTheTalent.WhoHasTheTalent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class DancerTest {

	@Test
	public void testAnnounce1() {
		assertEquals("Pop - 23453 - dancer", new Dancer(23453, "Pop").announce());
	}

	@Test
	public void testAnnounce2() {
		assertEquals("Pop - 435 - dancer", new Dancer(435, "Pop").announce());
	}

	@Test
	public void testAnnounce3() {
		assertNotEquals("Pop - 1435 - dancer", new Dancer(435, "Pop").announce());
	}

	@Test
	public void getDanceStyle() {
		assertEquals("Pop", new Dancer(435, "Pop").getDanceStyle());
	}

	@Test
	public void getDanceStyle2() {
		assertNotEquals("Hip Hop", new Dancer(435, "Pop").getDanceStyle());
	}
}
