package com.assignment.WhoHasTheTalent.WhoHasTheTalent;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Test;

public class CreateAuditionTest {

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateAudition() {
        List<String> expected = Arrays.asList("234 - performer", "1234 - performer", "11234 - performer","111234 - performer","Pop - 234 - dancer","Hip Hop - 234 - dancer","I Sing in the key of - P - 234");
        assertEquals(expected, new CreateAudition().generateAudition());
	}

	@Test
	public void testGenerateAudition1() {
        List<String> expected = Arrays.asList("2234 - performer", "1234 - performer", "11234 - performer","111234 - performer","Pop - 234 - dancer","Hip Hop - 234 - dancer","I Sing in the key of - P - 234");
        assertNotEquals(expected, new CreateAudition().generateAudition());
	}
}
