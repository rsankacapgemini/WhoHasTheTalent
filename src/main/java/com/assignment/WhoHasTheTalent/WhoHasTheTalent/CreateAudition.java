package com.assignment.WhoHasTheTalent.WhoHasTheTalent;

import java.util.ArrayList;
import java.util.List;

public class CreateAudition {

	public List<String> generateAudition() {
		List<Performer> performers = new ArrayList<>();
		List<String> output = new ArrayList<>();
		performers.add(new Performer(234));
		performers.add(new Performer(1234));
		performers.add(new Performer(11234));
		performers.add(new Performer(111234));
		performers.add(new Dancer(234, "Pop"));
		performers.add(new Dancer(234, "Hip Hop"));
		performers.add(new Vocalist(234, "P"));
		for (Performer performer : performers) {
			output.add(performer.announce());
		}
		return output;

	}

}
