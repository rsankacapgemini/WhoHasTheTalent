package com.assignment.WhoHasTheTalent.WhoHasTheTalent;

public class Vocalist extends Performer {

	private String key;

	public Vocalist(int unionId, String key) {
		super(unionId);
		this.key = key;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getUnionId() {
		// TODO Auto-generated method stub
		return super.getUnionId();
	}

	public String getKey() {
		return key;
	}

	@Override
	public String announce() {
		// TODO Auto-generated method stub
		return "I Sing in the key of - " + this.getKey() + " - " + this.getUnionId();
	}

	public String announce(int volume) {
		if(volume<1 || volume >10) {
			throw new IllegalArgumentException("Invalid voulume range is from 1 to 10");
		}
		return "I Sing in the key of - " + this.getKey() + " - at the volume " + volume + " - "+ this.getUnionId();
	}

}
