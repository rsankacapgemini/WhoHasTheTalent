package com.assignment.WhoHasTheTalent.WhoHasTheTalent;

public class Dancer extends Performer {

	private String danceStyle;

	public Dancer(int unionId, String danceStyle) {
		super(unionId);
		this.danceStyle = danceStyle;
		// TODO Auto-generated constructor stub
	}

	public String getDanceStyle() {
		return danceStyle;
	}

	@Override
	public int getUnionId() {
		// TODO Auto-generated method stub
		return super.getUnionId();
	}

	@Override
	public String announce() {
		// TODO Auto-generated method stub
		return danceStyle + " - " + this.getUnionId() + " - dancer";
	}

}
